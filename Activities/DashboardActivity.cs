﻿using System;
using System.Collections.Generic;
using System.Linq;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V4.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Autofac;
using PortableDeviceMarketplace.Activities;
using AlertDialog = Android.Support.V7.App.AlertDialog;
using SupportToolbar = Android.Support.V7.Widget.Toolbar;

namespace PortableDeviceMarketplace
{
    [Activity(Label = "PortableDeviceMarketplace", MainLauncher = false, Theme ="@style/MyTheme")]
    public class DashboardActivity : AppCompatActivity, IListItemClickListener
    {
        SupportToolbar mToolBar;

        protected string homeTab = "Home";
        protected string mobilePhonesTab = "Mobile Phones";
        protected string tabletsTab = "Tablets";
        protected string basketTab = "Basket";

        protected ActionBarDrawerToggle mDrawerToggle;
        protected DrawerLayout mDrawerLayout;
        protected ListView mLeftDrawer;
        protected ArrayAdapter mNavigationPageAdapter;
        protected List<String>navigationPages;
        protected ProductList productList = new ProductList();

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            SetContentView(Resource.Layout.DashboardScreen);

            mLeftDrawer = FindViewById<ListView>(Resource.Id.Left_Drawer);
            mToolBar = FindViewById<SupportToolbar>(Resource.Id.toolbar);
            mDrawerLayout = FindViewById<DrawerLayout>(Resource.Id.Draw_Layout);

            SetSupportActionBar(mToolBar);

            navigationPages = new List<string>
            {
                homeTab,
                mobilePhonesTab,
                tabletsTab,
                basketTab
            };

            mNavigationPageAdapter = new ArrayAdapter<string>(this, Android.Resource.Layout.SimpleListItem1, navigationPages);
            mLeftDrawer.Adapter = mNavigationPageAdapter;

            mDrawerToggle = new ActionBarDrawerToggle(
                this,                       //Host Activity
                mDrawerLayout,              //Drawer Layout    
                0,                          //No open drawer content
                0                           //No closed drawer content
            );

            mDrawerLayout.AddDrawerListener(mDrawerToggle);
            mDrawerToggle.SyncState();

            SupportActionBar.SetHomeButtonEnabled(true);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetTitle(Resource.String.dashboardTitle);

            var mRecyclerView = FindViewById<RecyclerView>(Resource.Id.ProductListRecyclerView);
            var mLayoutManager = new LinearLayoutManager(this);
            mRecyclerView.SetLayoutManager(mLayoutManager);

            var mAdapter = new ItemsAdapter(productList.Products.Select(p => p.Name).ToList(), this);
            mRecyclerView.SetAdapter(mAdapter);

            mLeftDrawer.ItemClick += MLeftDrawer_ItemClick;
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            mDrawerToggle.OnOptionsItemSelected(item);
            return base.OnOptionsItemSelected(item);
        }

        public void OnClick(int position )
        {
            var SelectedId = productList.Products.Select(p => p.ID).ToList()[position];

            var products = App.Container.Resolve<IProductList>();

            //If the stored item id is the same as whats been selected, dont create new object
            if (products.ReturnsClickedItemId() != SelectedId)
            {
                products.UpdateClickedItem(SelectedId);
            }

            StartActivity(new Intent(this, typeof(ProductDetailActivity)));
        }

        void MLeftDrawer_ItemClick(object sender, AdapterView.ItemClickEventArgs e)
        {
            var positionOfItemClicked = e.Id;

            switch (positionOfItemClicked)
            {
                    //Navigate to home 
                case 0:
                    //Start Dashboard activity
                    StartActivity(new Intent(this, typeof(DashboardActivity)));
                    break;
                    //Mobile Phones
                case 1:
                    //Start MobilePhones activity
                    ShowAlert("Start Mobile Phones Activity, not done this yet");
                  break;
                    //Tablets
                case 2:
                    //Start Tablets activity
                    ShowAlert("Start Tablets Activity, not done this yet");
                   break;
                    //Basket
                case 3:
                    StartActivity(new Intent(this, typeof(BasketPageActivity)));
                    break;
            }
        }

        void ShowAlert(string alertMessage)
        {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
            alertDialog.SetTitle("Item Clicked");
            alertDialog.SetMessage(alertMessage);
            alertDialog.SetNeutralButton("Continue", delegate {
                alertDialog.Dispose();
            });
            alertDialog.Show();
       }
    }
}