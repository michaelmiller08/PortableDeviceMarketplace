﻿namespace PortableDeviceMarketplace
{
    public interface IProductList
    {
        void UpdateClickedItem(string Id);

        string ReturnsClickedItemName();

        int ReturnsProductImagePath();

        string ReturnsProductDetailDescription();

        string ReturnsClickedItemId();

        Product ReturnsClickedItem();
    }
}
