﻿using System.Linq;
using Android.OS;
using Android.Support.V4.App;
using Android.Views;
using Android.Widget;
using Autofac;
using PortableDeviceMarketplace.Activities;

namespace PortableDeviceMarketplace.Fragments
{
    public class BasketPageFragment : Fragment
    {
        protected TextView itemTextView;

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            ((FragmentHolderActivity)Activity).SetActionBarTitle(Resource.String.basketPageTitle);
        }

        public override void OnResume()
        {
            base.OnResume();
            ((FragmentHolderActivity)Activity).SetActionBarTitle(Resource.String.basketPageTitle);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
        {
            View view = inflater.Inflate(Resource.Layout.BasketPageScreen, container, false);

            itemTextView = view.FindViewById<TextView>(Resource.Id.basketPageTextView);

            var basket = App.Container.Resolve<IBasket>();

            if (basket.BasketHasProducts() == true)
            {
                string displayItemsText = string.Empty;

                foreach (var item in basket.GetProducts())
                {
                    int quantity = basket.GetQuantity(basket.GetProducts().ToList().FirstOrDefault(p => p.Name == item.Name));
                    displayItemsText += $"{quantity} {item.Name} in cart \n";
                }
                itemTextView.Text = displayItemsText;
            }
            else
            {
                itemTextView.Gravity = GravityFlags.Center;
                itemTextView.Text = "No Items to display!";
            }
            return view;
        }
    }
}
