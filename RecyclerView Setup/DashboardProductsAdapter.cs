﻿using System.Collections.Generic;
using Android.Support.V7.Widget;
using Android.Views;
using PortableDeviceMarketplace;

public class DashboardProductsAdapter : RecyclerView.Adapter
        {
            IList<string> _items;
            IProductClickListener _clickListener;

            public DashboardProductsAdapter(IList<string> items, IProductClickListener clickListener)
            {
                _items = items;
                _clickListener = clickListener;
            }

            public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
            {
                View itemView = LayoutInflater.From(parent.Context).
                                              Inflate(Resource.Layout.ProductItemView, parent, false);
                
                DashboardProductHolder vh = new DashboardProductHolder(itemView, _clickListener);
                return vh;
            }

            public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
            {
                DashboardProductHolder vh = holder as DashboardProductHolder;

                vh.Setup(_items[position], position);
            }

            public override int ItemCount
            {
                get { return _items.Count; }
            }
        }