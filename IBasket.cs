﻿using System.Collections.Generic;

namespace PortableDeviceMarketplace
{
    public interface IBasket
    {
        void AddToBasket();

        List<Product> GetProducts();

        List<string>GetProductIds();

        int GetQuantity(Product product);

        void RemoveFromBasket(Product product, int quantity);

        bool BasketHasProducts();
    }
}
